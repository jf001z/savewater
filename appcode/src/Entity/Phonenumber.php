<?php

namespace App\Entity;


class Phonenumber
{

    private $phonenumberId;
    private $userId;
    private $phonenumber;
    private $user;

    /**
     * @return mixed
     */
    public function getPhonenumberId()
    {
        return $this->phonenumberId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getPhonenumber()
    {
        return $this->phonenumber;
    }

    /**
     * @param mixed $phonenumber
     */
    public function setPhonenumber($phonenumber): void
    {
        $this->phonenumber = $phonenumber;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

}

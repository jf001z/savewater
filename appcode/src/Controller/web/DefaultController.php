<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 04/07/2018
 * Time: 20:36
 */

namespace App\Controller\web;



use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="default")
     * @method("GET")
     */
    public function indexAction(Request $request){
        $num1 = 11;
        $num2 = 22;
        if($request->query->has('num_1')&&$request->query->has('num_1')){
            $num1 = $request->query->get('num_1');
            $num2 = $request->query->get('num_2');
        }
        $result = $this->get('UserService')->getSum($num1,$num2);
        return new JsonResponse(array('result'=>$result));
    }

    /**
     * @Route("/test", name="test")
     * @method("GET")
     */
    public function testAction(Request $request){

        return new Response($this->renderView('test.html.twig',array('title'=>'test')),200,array());
    }
    /**
     * @Route("/expression", name="expression")
     * @method("GET")
     */
    public function expressionAction(Request $request){

        $ex = new ExpressionLanguage();
        $test = 35;
        $test1 = 32;
        $result1 = $ex->evaluate('1 == 1');
        $result2 = $ex->evaluate('18+12');
        $result3 = $ex->evaluate(
            'test*test1',
            array(
                'test' => $test,
                'test1' => $test1
            )
        );
        $result4 = $ex->evaluate(
        'test==1',
            array(
                'test' => $test
            )
        );
        $result5 = $ex->evaluate(
        'test1==2',
            array(
                'test1' => $test1
            )
        );

        return new Response($this->renderView(
            'expression.html.twig',
            array('title'=>'test',
            'results'=>array(
                '1==1'=>$result1,
                '18+12'=>$result2,
                'test*test1'=>$result3,
                'test==1'=>$result4==false?'False':'True',
                'test1==2'=>$result5==false?'False':'True'
            ))),
            200,array());
    }

}
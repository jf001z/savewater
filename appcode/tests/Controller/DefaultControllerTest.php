<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 05/07/2018
 * Time: 21:20
 */

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Routing\AnnotatedRouteControllerLoader;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testDefaultController(){
        $client = static::createClient();

        $client->request('GET', '/test');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $crawler = $client->request('GET', '/test');

        $this->assertGreaterThan(
            0,
            $crawler->filter('html:contains("test")')->count()
        );
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 05/07/2018
 * Time: 20:54
 */

namespace App\Tests\Service;


use App\Service\UserService;
use PHPUnit\Framework\TestCase;

class getSumTest extends TestCase
{
    public function testSum(){
        $userService = new UserService();
        $result = $userService->getSum(34,48);
        $this->assertEquals(82,$result);
    }

}
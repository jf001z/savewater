# README #


### What is this repository for? ###

This repository is for the test of the job application for saveWaterSaveEnergy.

###  How do I get set up? ###

After downloaded the repository, go into the appcode folder,
please run 
```
Composer install
```
This is to install the components used in the program.

###  Details for each questions ###

Question 1: is defined in appcode/config/services.yaml and the
class is defined in /appcode/src/Services/UserService.php

Question 2: orm is defined in appcode/config/packages/doctrine.yaml
I used yaml file to define the orm, which can be found in 
appcode/src/Entity.

Question 3: all of the tests can be found in appcode/tests, two
test, 3 assetions have been created.

Question 4: The answer of this question is in appcode/src/
Controller/web/DefaultController.php fucntion: expressionAction()

